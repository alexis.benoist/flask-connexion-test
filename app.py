from flasgger import Swagger
from flask import Flask, request, jsonify
import random

COUNTER = 0


def set_counter(value):
    global COUNTER
    COUNTER = value


def get_counter():
    return jsonify({'value': COUNTER})


def get_random_int():
    return random.randint(1, 10)


def counter_add_random_inner():
    set_counter(COUNTER + get_random_int())
    return COUNTER


app = Flask('counter')
swagger = Swagger(app, config={**Swagger.DEFAULT_CONFIG, "specs_route": "/ui/"}, template_file='swagger/counter.yaml')


@app.route('/counter/add/random', methods=('POST',))
def counter_add_random():
    counter_add_random_inner()
    return get_counter()


@app.route('/counter', methods=('PUT', 'GET'))
def counter():
    if request.method == 'PUT':
        increment = int(request.args.get('value', 1))
        set_counter(COUNTER + increment)
    return get_counter()


if __name__ == '__main__':
    app.run(port=8080)
