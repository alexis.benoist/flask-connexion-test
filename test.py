import pytest
from flask import request
from flask.testing import FlaskClient
from unittest.mock import patch

from app import app, set_counter, counter_add_random_inner


def setup_function():
    set_counter(0)


@pytest.fixture
def client():
    app.testing = True
    client: FlaskClient = app.test_client()
    yield client


def test_get(client):
    rv = get_counter(client)
    check_value(rv, 0)


def get_counter(client):
    return client.get('/counter')


def check_value(rv, expected_value):
    assert 200 == rv.status_code
    assert expected_value == rv.json['value']


def test_update(client):
    rv = increment_counter(client)
    check_value(rv, 1)


def increment_counter(client):
    return client.put('/counter')


def test_update_twice(client):
    rv = increment_counter(client)
    check_value(rv, 1)

    rv = increment_counter(client)
    check_value(rv, 2)


def test_update_with_value(client):
    with app.test_client() as c:
        rv = c.put('/counter?value=2')
        assert '2' == request.args['value']
        check_value(rv, 2)


@patch('app.counter_add_random_inner', return_value=2)
def test_increase_random(mock, client):
    client.post('/counter/add/random')
    assert 1 == mock.call_count


def test_increase_random_logic():
    counter = counter_add_random_inner()
    assert 0 < counter <= 10


def test_swagger(client):
    rv = client.get('/apispec_1.json')
    assert '2.0' == rv.json['swagger']
